'''
Created on 2013-11-10

@author: Smitty
'''
import pygame

class Direction:
  DOWN = 0
  LEFT = 1
  RIGHT = 2
  UP = 3

class HitBox:
  
 def __init__(self, x1, y1, x2, y2):
    self.x1 = x1
    self.y1 = y1
    self.x2 = x2
    self.y2 = y2

class Creature(object):
  
  def __init__(self, screen):
    self.mobile = False
    self.step_length = 5
    self.init_speed = 5
    self.speed = self.init_speed
    self.hp = 1
    self.mp = 1
    self.x = 300
    self.y = 300
    self.sprite = None
    self.sprite_x = 0 #32 each
    self.sprite_y = 0 #48 each
    self.sprite_width = 32
    self.sprite_height = 48
    #self.hitbox = HitBox(300 + (self.sprite_width * 0.2), 300 + self.sprite_height, 295 + (self.sprite_width * 0.8), 300 + (self.sprite_height - 5))
    self.sprite_walk_count = 2 #must be 1 less than the actual image because sprites start at x and y 0
    self.screen = screen
    self.Teleport(300, 300)
    
  def Teleport(self, x, y):
    self.x = x
    self.y = y
    self.hitbox = HitBox(
                         x + (self.sprite_width * 0.2), 
                         y + self.sprite_height, 
                         (x - self.step_length) + (self.sprite_width * 0.8),
                          y + (self.sprite_height - self.step_length)
                        )

  def Move(self, direction, canAdvance, simulate = False):     
    tmpX = self.x
    tmpY = self.y
    tmpHitbox = HitBox(self.hitbox.x1, self.hitbox.y1, self.hitbox.x2, self.hitbox.y2)
    
    if(direction == Direction.LEFT):
      tmpX -= self.step_length
      tmpHitbox.x1 -= self.step_length
      tmpHitbox.x2 -= self.step_length
    elif(direction == Direction.RIGHT):
      tmpX += self.step_length
      tmpHitbox.x1 += self.step_length
      tmpHitbox.x2 += self.step_length
    elif(direction == Direction.UP):
      tmpY -= self.step_length
      tmpHitbox.y1 -= self.step_length
      tmpHitbox.y2 -= self.step_length
    elif(direction == Direction.DOWN):
      tmpY += self.step_length
      tmpHitbox.y1 += self.step_length
      tmpHitbox.y2 += self.step_length
      
    if(simulate):
      return tmpHitbox
    else:
      self.speed -= 1
  
    if(self.speed == 0):
      self.speed = self.init_speed
      
      self.sprite_y = direction * self.sprite_height
      
      if(self.sprite_x == self.sprite_width * self.sprite_walk_count):
        self.sprite_x = 0 
      else:
        self.sprite_x = self.sprite_x + self.sprite_width
      
      if(canAdvance):
        self.x = tmpX
        self.y = tmpY
        self.hitbox = tmpHitbox
      