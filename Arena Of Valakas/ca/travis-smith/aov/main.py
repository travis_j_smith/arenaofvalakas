'''
Created on 2013-11-10

@author: Smitty
'''
import pygame
from player import Player
from creature import Direction
from map import Map
from draw import Draw
import os
from pygame.locals import QUIT
from pygame.locals import KEYDOWN
from pygame.locals import K_LEFT
from pygame.locals import K_RIGHT
from pygame.locals import K_DOWN
from pygame.locals import K_UP
 
class Game:
                
  def Initialize(self):
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.init()
    self.screen = pygame.display.set_mode((1020, 760))
    pygame.display.set_caption('Arena of Valakas')
    pygame.mouse.set_visible(0)
    self.clock = pygame.time.Clock()
    self.map = Map(self.screen, "002")
    self.drawer = Draw(self.screen, self.map)

  def Run(self):
    player = Player(self.screen)
    
    while 1:
      self.clock.tick(60)
      for event in pygame.event.get():
        if event.type == QUIT:
         return
      
      self.drawer.Draw(player)
      
      keys = pygame.key.get_pressed()
      
      if keys[K_DOWN]:
         player.Move(Direction.DOWN, 
                     self.map.Passable(player, Direction.DOWN))
      elif keys[K_UP]:
          player.Move(Direction.UP, 
                      self.map.Passable(player, Direction.UP))
      elif keys[K_LEFT]:
          player.Move(Direction.LEFT, 
                      self.map.Passable(player, Direction.LEFT))
      elif keys[K_RIGHT]:
          player.Move(Direction.RIGHT, 
                      self.map.Passable(player, Direction.RIGHT))
    
      if(self.map.change):
        player.Teleport(100, 100)
        self.map = Map(self.screen, self.map.changeMap)
        self.drawer = Draw(self.screen, self.map)
        
      pygame.display.update()
         
if __name__ == '__main__':
  aov = Game()
  aov.Initialize()
  aov.Run()