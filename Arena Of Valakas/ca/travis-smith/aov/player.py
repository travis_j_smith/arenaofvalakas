'''
Created on 2013-11-10

@author: Smitty
'''
import pygame
from creature import Creature

class CLASS:
  KNIGHT = 1
  WIZARD = 2
  ROGUE = 3
  
class GENDER:
  MALE = 1
  FEMALE = 2

class Player(Creature):

  def __init__(self, screen):
    Creature.__init__(self, screen) 
    self.gender = GENDER.MALE
    self.Class = CLASS.KNIGHT
    self.mobile = True
    self.sprite = pygame.image.load("characters.png").convert_alpha()