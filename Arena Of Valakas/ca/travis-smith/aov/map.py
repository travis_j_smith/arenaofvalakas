'''
Created on 2013-11-10

@author: Smitty
'''
import pygame
import math
from creature import Direction
from xml.etree.ElementTree import ElementTree

class Tileset(object):
  
  def __init__(self, name, id, src):
    self.name = name
    self.id = id
    self.src = src
    self.sprite = pygame.image.load(src).convert_alpha()
    self.tiles = {}

class TileConfig(object):
  
  def __init__(self, id, passable, x, y, width, height):
    self.id = id
    self.passable = passable
    self.startx = int(x)
    self.starty = int(y)
    self.width = int(width)
    self.height = int(height)
    self.pass_config = {}

class Tile(object):
  def __init__(self, x_start, y_start, x_end, y_end, tileset_id, tile_id):
    self.x_start = int(x_start)
    self.x_end = self.x_start + int(x_end)
    self.y_start = int(y_start)
    self.y_end = self.y_start + int(y_end)
    self.tileset_id = tileset_id
    self.tile_id = tile_id
    self.exitPoint = False
    self.exitPointMap = ""

class Map(object):
  grid_size = 10
  
  def Passable(self, creature, direction):
    returnValue = True  
    
    next_step = creature.Move(direction, True, True)
    
    if(self._check_collision(self.maptiles, next_step, creature.step_length) or 
    self._check_collision(self.maptiles_overlay, next_step, creature.step_length)):
      returnValue = False
    
    return returnValue

  def _check_collision(self, tiles, hitbox, step_length):
    returnValue = False

    for key in tiles:
      current = tiles[key]
      currentTileset = self.tilesets[current.tileset_id]
      currentTileConfig = currentTileset.tiles[current.tile_id]
      
      if(hitbox.x2 >= current.x_start and hitbox.x1 <= current.x_end 
         and hitbox.y2 >= current.y_start and hitbox.y2 <= current.y_end):
          
        if(not currentTileConfig.passable):
          left_hit = math.floor((current.x_end - hitbox.x1) - step_length)
          down_hit = math.floor((hitbox.y2 - current.y_start) - step_length)
                  
          if(len(currentTileConfig.pass_config) == 0):
            returnValue = True
          else:
            #it is possible for the user to get < step_length into the block before collision detection is fired
            #so set it so the first position if it is less than the first position
            if(left_hit < 0): left_hit = 0
            if(down_hit < 0): down_hit = 0
            
            if(currentTileConfig.pass_config[math.floor(down_hit / 5)][math.floor(left_hit / 5)] == 0):
              returnValue = True
            else:
              returnValue = False
        elif(current.exitPoint):
         self.change = True
         self.changeMap = current.exitPointMap
         
    return returnValue    
  
  def __init__(self, screen, id):
      
    #these get set when loading sprites, to know how far to draw for the overlays
    self.max_sprite_width = 0 
    self.max_sprite_height = 0
    self.min_sprite_width = 0
    self.min_sprite_height = 0
    self.change = False
    tree = ElementTree()
    tree.parse("map_sprites.xml")
    
    self.tilesets = {}
    
    for tileset in tree.findall("Tileset"):
      tmpSet = Tileset(tileset.get("name"), tileset.get("id"), tileset.get("src"))
      for tile in tileset.findall("Tile"):
        tmpConfig = TileConfig(
                              tile.get("id"),
                              tile.get("Passable").upper() == "TRUE",
                              tile.find("xstart").text,
                              tile.find("ystart").text,
                              tile.find("width").text,
                              tile.find("height").text
                              )
        #check to see if the sprite has pass_config and if it does, load it
        tmpPass_Config = tile.find("pass_config")
        if tmpPass_Config is not None:
          splitted = tmpPass_Config.text.strip().split("\n")
          for x in range(0, len(splitted)):
            tmpConfig.pass_config[x] = list(map(int, list(splitted[x].strip())))
          
        if(tmpConfig.width > self.max_sprite_width):
          self.max_sprite_width = tmpConfig.width
        
        if(tmpConfig.height > self.max_sprite_height):
          self.max_sprite_height = tmpConfig.height
          
        if(tmpConfig.height < self.min_sprite_height or self.min_sprite_height == 0):
          self.min_sprite_height = tmpConfig.height
          
        if(tmpConfig.width < self.min_sprite_width or self.min_sprite_width == 0):
          self.min_sprite_width = tmpConfig.width
        
        if("Overlay" in tile.attrib and tile.get("Overlay").upper() == "TRUE"):
            tmpConfig.overlay = True
        else:
            tmpConfig.overlay = False
        
        tmpSet.tiles[tile.get("id")] = tmpConfig
      self.tilesets[tileset.get("id")] = tmpSet
      
    self.maptiles = {}
    self.maptiles_overlay = {}
    self.screen = screen
    
    tree.parse(id + ".txt")
    
    for tile in tree.findall("Tile"):
      key = tile.get("x") + "-" + tile.get("y")
      tileConfig = self.tilesets[tile.get("Tileset")].tiles[tile.get("Tile")]
      
      tmpTile = Tile(
                       tile.get("x"),
                       tile.get("y"),
                       tileConfig.width,
                       tileConfig.height,
                       tile.get("Tileset"),
                       tile.get("Tile")
                      )
      
      if("ExitPoint" in tile.attrib and tile.get("ExitPoint").upper() == "TRUE"):
          tmpTile.exitPoint = True
          tmpTile.exitPointMap = tile.get("ExitPointMap")
          
      if(tileConfig.overlay):
        self.maptiles_overlay[key] = tmpTile
      else:
        self.maptiles[key] = tmpTile