import pygame
from map import Map

class Draw(object):

  def __init__(self, screen, map):
    self.map = map
    self.initial_draw = True
    self.screen = screen
    
  def Draw(self, player, creature_array = None):
    self._DrawMap(player, creature_array)
    self._DrawCreature(player)
    self._DrawOverlays(player)
    
    if(creature_array is not None):
      for creature in creature_array:
        self._DrawCreature(creature)

  #draw the overlays. Objects that appear over the map and possibly creatures
  def _DrawOverlays(self, creature):
    if(creature.mobile):
     self.__LoadTexturesNearObject(self.map.maptiles_overlay, creature, True)

  #draw the map -- ignoring any overlays. We want to draw them in a different method
  def _DrawMap(self, player = None, creature_array = None):
    #if this is the initial load of the map, then draw the entire map
    if(self.initial_draw):
      self.initial_draw = False
      for tile in self.map.maptiles:
          currentTile = self.map.maptiles[tile]
          self.__DrawTexture(currentTile)
    
      for tile in self.map.maptiles_overlay:
        currentTile = self.map.maptiles_overlay[tile]
        self.__DrawTexture(currentTile)
    else:
      self.__LoadTexturesNearObject(self.map.maptiles, player)

  def _DrawCreature(self, creature):
    self.screen.blit(
                      creature.sprite,
                      (creature.x, creature.y), 
                      (creature.sprite_x, creature.sprite_y, creature.sprite_width, creature.sprite_height)
                     )
    
  #load the textures near the moving creature
  def __LoadTexturesNearObject(self, textureObject, creature, overlay = False):
      distance = 10
      
      if(overlay):
        for key in textureObject:
          current = textureObject[key]
          currentTileset = self.map.tilesets[current.tileset_id]
          currentTileConfig = currentTileset.tiles[current.tile_id]
          if(creature.x in range(
                                 current.x_start - (self.map.max_sprite_width + creature.sprite_width), 
                                 current.x_start + (self.map.max_sprite_width + creature.sprite_width)) 
             and creature.y in range(
                                     current.y_start - (self.map.max_sprite_height + creature.sprite_height), 
                                     current.y_start + (self.map.max_sprite_height + creature.sprite_height))):
            self.__DrawTexture(current)
            #if the creature is below the bottom of the texture, and within the x bounds of the texture
            #and creature is not more than the height of the creature away from the texture
            #redraw the player so they appear 'over' the texture
            if((creature.y + creature.sprite_height > current.y_end and
               creature.y + creature.sprite_height < current.y_end + creature.sprite_height) and
               (creature.x in range(current.x_start, current.x_end)
                or (creature.x + creature.sprite_width in range(current.x_start, current.x_end)))):
              self._DrawCreature(creature)
      else:
        for y in range(creature.y - creature.sprite_height - distance - 5, creature.y + creature.sprite_height + 5):
          for x in range(creature.x - creature.sprite_width - distance - 5, creature.x + creature.sprite_width + 5):
            key = str(x) + "-" + str(y)
            if key in textureObject:             
              self.__DrawTexture(textureObject[key])

  #get the actual texture from the map and draw it
  def __DrawTexture(self, currentTile):
    currentTileset = self.map.tilesets[currentTile.tileset_id]
    currentTileConfig = currentTileset.tiles[currentTile.tile_id]
    self.screen.blit(
                     currentTileset.sprite,
                     (currentTile.x_start, currentTile.y_start), 
                     (currentTileConfig.startx, currentTileConfig.starty, currentTileConfig.width, currentTileConfig.height)
                    )